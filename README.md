# Vnphp Morpher Bundle

An unofficial Symfony Bundle for http://morpher.ru/ws3/ api.
[![build status](https://gitlab.com/vnphp/morpher-bundle/badges/master/build.svg)](https://gitlab.com/vnphp/morpher-bundle/commits/master)

## Installation 

```
composer require vnphp/morpher-bundle
```

## Usage

```php
<?php

$declension = $this->container->get('vnphp_morpher.client_ru')
                    ->getDeclension('автомойки');
```