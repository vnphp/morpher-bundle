<?php


class MorpherClientTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var \Vnphp\MorpherBundle\Client\MorpherClient
     */
    protected $client;

    protected function setUp()
    {
        parent::setUp();
        $this->client = new \Vnphp\MorpherBundle\Client\MorpherClient('russian');
    }

    public function testGetDeclension()
    {
        $res = $this->client->getDeclension('автомойки');
        static::assertEquals('автомоек', $res['Р']);
    }
}
