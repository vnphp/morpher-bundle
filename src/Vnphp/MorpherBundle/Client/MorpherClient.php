<?php


namespace Vnphp\MorpherBundle\Client;

use Buzz\Browser;
use Buzz\Client\Curl;

class MorpherClient
{
    /**
     * @var string
     */
    protected $language;

    /**
     * @var Browser
     */
    protected $browser;

    /**
     * MorpherClient constructor.
     * @param $language
     */
    public function __construct($language)
    {
        $this->language = $language;

        $client = new Curl();
        $client->setTimeout(30);
        $this->browser = new Browser($client);
    }

    public function getDeclension($phrase)
    {
        $url = "https://ws3.morpher.ru/{$this->language}/declension?" . http_build_query([
                's' => $phrase,
            ]);
        $xml = new \SimpleXMLElement($this->browser->get($url)->getContent());

        $result = iterator_to_array($xml);
        $result = array_map(function (\SimpleXMLElement $element) {
            return (string)$element;
        }, $result);

        return $result;
    }
}
