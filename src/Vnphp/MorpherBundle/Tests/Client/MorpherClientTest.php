<?php


namespace Vnphp\MorpherBundle\Tests\Client;

use TwentyUa\CatalogBundle\Tests\WebTestCase;
use Vnphp\MorpherBundle\Client\MorpherClient;

class MorpherClientTest extends WebTestCase
{
    /**
     * @var MorpherClient
     */
    protected $client;

    protected function setUp()
    {
        parent::setUp();
        $this->client = static::createClient()
            ->getContainer()
            ->get('vnphp_morpher.client_ru');
    }

    public function testGetDeclension()
    {
        $res = $this->client->getDeclension('автомойки');
        static::assertEquals('автомоек', $res['Р']);
    }
}
